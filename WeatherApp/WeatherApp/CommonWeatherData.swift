//
//  CommonWeatherData.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/28/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import SwiftyJSON

class CommonWeatherData {
    private(set) var parsingSucceeded = true
    
    private(set) var id: Int = 0
    private(set) var name: String = ""
    private(set) var country: String = ""
    private(set) var cod: Int = 0
    private(set) var coord: (lon:Double, lat:Double) = (0,0)
    
    init(dataFromCurrent data: JSON) {
        id = data["id"].int ?? defaultIntValue()
        cod = data["cod"].int ?? defaultIntValue()
        name = data["name"].string ?? defaultStringValue()
        country = data["sys"]["country"].string ?? defaultStringValue()
        coord.lon = data["coord"]["lon"].double ?? defaultDoubleValue()
        coord.lat = data["coord"]["lat"].double ?? defaultDoubleValue()
    }
    
    init(dataFromForecast data: JSON) {
        id = data["city"]["id"].int ?? defaultIntValue()
        
        let codString = data["cod"].string ?? defaultStringValue()
        cod = Int(codString) ?? defaultIntValue()
        
        name = data["city"]["name"].string ?? defaultStringValue()
        country = data["city"]["country"].string ?? defaultStringValue()
        coord.lon = data["city"]["coord"]["lon"].double ?? defaultDoubleValue()
        coord.lat = data["city"]["coord"]["lat"].double ?? defaultDoubleValue()
    }
    
    private func defaultDoubleValue() -> Double {
        parsingSucceeded = false
        return 0.0
    }
    
    private func defaultIntValue() -> Int {
        parsingSucceeded = false
        return 0
    }
    
    private func defaultStringValue() -> String {
        parsingSucceeded = false
        return ""
    }
}
