//
//  CurrentWeatherData.swift
//  UIPageViewControllerDemo2
//
//  Created by Marko Zec on 10/21/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

class WeatherData {
    private(set) var parsingSucceeded = true
    
    private(set) var usesLocation = false
    private(set) var location: CLLocationCoordinate2D?
    
    private(set) var id: Int = 0
    private(set) var name: String = ""
    
    private(set) var commonWeatherData: CommonWeatherData?
    private var currentWeatherDataLastUpdated: CurrentHourWeatherData?
    private(set) var hourlyForecastWeatherData = [Int:OneHourWeatherData?]() // [unix time : OneHourWeatherData]
    private(set) var dailyForecastWeatherData = [Int:OneDayWeatherData?]() // [unix time : OneDayWeatherData]
    
    var currentWeatherData: CurrentHourWeatherData? {
        var data: CurrentHourWeatherData?
        
        if let currentData = currentWeatherDataLastUpdated,
            let firstNextHour = firstNextHourFromFourlyForecast
        {
            if currentData.dt + 3600*2 < Int(Date().timeIntervalSince1970.rounded()) {
                data = CurrentHourWeatherData(oneHourData: firstNextHour, oldCurrentData: currentData)
                print("Current is generated from forecast for \(commonWeatherData?.name)")
            } else {
                data = currentWeatherDataLastUpdated
                print("Current is lastest update \(commonWeatherData?.name)")
            }
        }
        
        return data
    }
    
    var firstNextHourFromFourlyForecast: OneHourWeatherData? {
        return getOneHourFromHourlyForecast(fromTime: Int(Date().timeIntervalSince1970))
    }
    
    var hourlyForecastWeatherDataForNextMax20Hours: [Int:OneHourWeatherData?]? {
        return getHourlyForecastFromTime(from: Int(Date().timeIntervalSince1970), numberOf3HourForecasts: 20)
    }
    var dailyForecastWeatherDataForNextMax10Days: [Int:OneDayWeatherData?]? {
        return getDailyForecastFromTime(from: Int(Date().timeIntervalSince1970), numberOfDays: 10)
    }
    
    private(set) var currentWeatherDataLastUpdate: Int = 0
    private(set) var hourlyForecastWeatherDataLastUpdate: Int = 0
    
    init(usesLocation: Bool) {
        parsingSucceeded = false
        self.usesLocation = true
    }
    
    init(getDataForLoactionID id: Int) {
        parsingSucceeded = true
        setCurrentWeatherDataFromOWM(cityID: id)
        setHourlyForecastWeatherDataFromOWM(cityID: id)
        setDailyForecastWeatherDataFromOWM(cityID: id)
    }
    
    func setLocation(location: CLLocationCoordinate2D) {
        self.location = location
    }
    
    func setDataForCurrentLocation() {
        if let location = self.location {
            parsingSucceeded = true
            setCurrentWeatherDataFromOWM(location: location)
            setHourlyForecastWeatherDataFromOWM(location: location)
            setDailyForecastWeatherDataFromOWM(location: location)
        }
    }
    
    private func setCurrentWeatherDataFromOWM(location: CLLocationCoordinate2D) {
        if let data = getCurrentWeatherDataFromOWM(location: location) {
            self.id = data.commonWeatherData.id
            self.name = data.commonWeatherData.name
            self.commonWeatherData = data.commonWeatherData
            self.currentWeatherDataLastUpdated = data.currentHourWeatherData
        }
    }
    
    private func setCurrentWeatherDataFromOWM(cityID id: Int) {
        if let data = getCurrentWeatherDataFromOWM(cityID: id) {
            self.id = data.commonWeatherData.id
            self.name = data.commonWeatherData.name
            self.commonWeatherData = data.commonWeatherData
            self.currentWeatherDataLastUpdated = data.currentHourWeatherData
        }
    }
    
    private func setHourlyForecastWeatherDataFromOWM(location: CLLocationCoordinate2D) {
        if let data = getHourlyForecastWeatherDataFromOWM(location: location) {
            self.hourlyForecastWeatherData = data.hourlyForecastWeatherData
        }
    }
    
    private func setHourlyForecastWeatherDataFromOWM(cityID id: Int) {
        if let data = getHourlyForecastWeatherDataFromOWM(cityID: id) {
            self.hourlyForecastWeatherData = data.hourlyForecastWeatherData
        }
    }
    
    private func setDailyForecastWeatherDataFromOWM(location: CLLocationCoordinate2D) {
        if let data = getDailyForecastWeatherDataFromOWM(location: location) {
            self.dailyForecastWeatherData = data.dailyForecastWeatherData
        }
    }
    
    private func setDailyForecastWeatherDataFromOWM(cityID id: Int) {
        if let data = getDailyForecastWeatherDataFromOWM(cityID: id) {
            self.dailyForecastWeatherData = data.dailyForecastWeatherData
        }
    }
    
    private func getCurrentWeatherDataFromOWM(location: CLLocationCoordinate2D) -> (commonWeatherData:CommonWeatherData, currentHourWeatherData:CurrentHourWeatherData)? {
        if let json = JSONFromOWM.getWeather(forecastType: .Current, location: location) {
            if let parsedData = parseCurrentWeatherData(forJSON: json) {
                return parsedData
            }
        }
        
        self.parsingSucceeded = false
        return nil
    }
    
    private func getCurrentWeatherDataFromOWM(cityID id: Int) -> (commonWeatherData:CommonWeatherData, currentHourWeatherData:CurrentHourWeatherData)? {
        if let json = JSONFromOWM.getWeather(forecastType: .Current, cityID: id) {
            if let parsedData = parseCurrentWeatherData(forJSON: json) {
                return parsedData
            }
        }
        
        self.parsingSucceeded = false
        return nil
    }
    
    private func getHourlyForecastWeatherDataFromOWM(location: CLLocationCoordinate2D) -> (commonWeatherData:CommonWeatherData, hourlyForecastWeatherData:[Int:OneHourWeatherData?])? {
        if let json = JSONFromOWM.getWeather(forecastType: .HourlyForecast, location: location) {
            if let parsedData = parseHourlyForecastWeatherData(forJSON: json) {
                return parsedData
            }
        }
        
        self.parsingSucceeded = false
        return nil
    }
    
    private func getHourlyForecastWeatherDataFromOWM(cityID id: Int) -> (commonWeatherData:CommonWeatherData, hourlyForecastWeatherData:[Int:OneHourWeatherData?])? {
        if let json = JSONFromOWM.getWeather(forecastType: .HourlyForecast, cityID: id) {
            if let parsedData = parseHourlyForecastWeatherData(forJSON: json) {
                return parsedData
            }
        }
        
        self.parsingSucceeded = false
        return nil
    }
    
    private func getDailyForecastWeatherDataFromOWM(location: CLLocationCoordinate2D) -> (commonWeatherData:CommonWeatherData, dailyForecastWeatherData:[Int:OneDayWeatherData?])? {
        if let json = JSONFromOWM.getWeather(forecastType: .DailyForecast, location: location) {
            if let parsedData = parseDailyForecastWeatherData(forJSON: json) {
                return parsedData
            }
        }
        
        self.parsingSucceeded = false
        return nil
    }
    
    private func getDailyForecastWeatherDataFromOWM(cityID id: Int) -> (commonWeatherData:CommonWeatherData, dailyForecastWeatherData:[Int:OneDayWeatherData?])? {
        if let json = JSONFromOWM.getWeather(forecastType: .DailyForecast, cityID: id) {
            if let parsedData = parseDailyForecastWeatherData(forJSON: json) {
                return parsedData
            }
        }
        
        self.parsingSucceeded = false
        return nil
    }
    
    private func parseCurrentWeatherData(forJSON json: JSON) -> (commonWeatherData:CommonWeatherData, currentHourWeatherData:CurrentHourWeatherData)? {
        let commonData = CommonWeatherData(dataFromCurrent: json)
        let currentHourData = CurrentHourWeatherData(data: json)
        
        if commonData.parsingSucceeded && currentHourData.parsingSucceeded {
            return (commonData, currentHourData)
        } else {
            print("Error: Parsing JSON failed for Current Weather, structure is modified!")
        }
        
        return nil
    }
    
    private func parseHourlyForecastWeatherData(forJSON json: JSON) -> (commonWeatherData:CommonWeatherData, hourlyForecastWeatherData:[Int:OneHourWeatherData?])? {
        let commonData = CommonWeatherData(dataFromForecast: json)
        let listOfHourlyData = ListOfOneHourWeatherData(data: json)
        
        if commonData.parsingSucceeded && listOfHourlyData.parsingSucceeded {
            return (commonData, listOfHourlyData.list)
        } else {
            print("Error: Parsing JSON failed for 5 days / 3 hours Forecast Weather, structure is modified!")
        }
        
        return nil
    }
    
    private func parseDailyForecastWeatherData(forJSON json: JSON) -> (commonWeatherData:CommonWeatherData, dailyForecastWeatherData:[Int:OneDayWeatherData?])? {
        let commonData = CommonWeatherData(dataFromForecast: json)
        let listOfDailyData = ListOfOneDayWeatherData(data: json)
        
        if commonData.parsingSucceeded && listOfDailyData.parsingSucceeded {
            return (commonData, listOfDailyData.list)
        } else {
            print("Error: Parsing JSON failed for 16 days Forecast Weather, structure is modified!")
        }
        
        return nil
    }
    
    private func getHourlyForecastFromTime(from: Int, numberOf3HourForecasts: Int) -> [Int:OneHourWeatherData?]? {
        
        // get closest hour from forecast data, in 3hour forecast
        let requsetedHour = from - (from % 3600)
        var approximateHour = 0
        var found = false
        for hour in 0...12 {
            if !found {
                let nextHour = requsetedHour + hour * 3600
                let keyExists = hourlyForecastWeatherData[nextHour] != nil
                if keyExists {
                    approximateHour = nextHour
                    found = true
                }
            }
        }
        
        // if next hour is found in forecast
        if found {
            var requestedForecastDictionary = [Int:OneHourWeatherData?]() // new dictionary with maximum of numberOf3HourForecasts elements
            
            for number in 0..<numberOf3HourForecasts {
                let next3Hour = approximateHour + number * 3600 * 3
                if let hour = hourlyForecastWeatherData[next3Hour] {
                    requestedForecastDictionary[next3Hour] = hour
                }
            }
            
            return requestedForecastDictionary
        }
        
        return nil
    }
    
    private func getOneHourFromHourlyForecast(fromTime: Int) -> OneHourWeatherData? {
        
        if let array = getHourlyForecastFromTime(from: fromTime, numberOf3HourForecasts: 1) {
            for (_,data) in array {
                if let oneHour = data {
                    return oneHour
                }
            }
        }
        
        return nil
    }
    
    private func getDailyForecastFromTime(from: Int, numberOfDays: Int) -> [Int:OneDayWeatherData?]? {
        
        let oneDayInterval = 86400
        var nextDayExists = false
        var nextDay = 0
        for (day,_) in dailyForecastWeatherData {
            if !nextDayExists {
                if from < day && from + oneDayInterval > day {
                    nextDay = day
                    nextDayExists = true
                }
            }
        }
        
        if nextDayExists {
            var requestedForecastDictionary = [Int:OneDayWeatherData?]() // new dictionary with maximum of numberOfDays elements
            
            for (day,data) in dailyForecastWeatherData {
                if day >= nextDay && day <= from + oneDayInterval * numberOfDays {
                    requestedForecastDictionary[day] = data
                }
            }
            
            return requestedForecastDictionary
        }
        
        return nil
    }
    
    
}
