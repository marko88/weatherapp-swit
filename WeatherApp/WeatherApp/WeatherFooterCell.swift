//
//  WeatherFooterCell.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/3/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class WeatherFooterCell: UICollectionViewCell {
    
    var weatherData: WeatherData?
    
    let borderWidth = CGFloat(1)
    let buttomBorder = CALayer()
    let topBorder = CALayer()
    
    @IBOutlet weak var lblSunrise: UILabel!
    @IBOutlet weak var lblSunset: UILabel!
    @IBOutlet weak var lblClouds: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblWind: UILabel!
    @IBOutlet weak var lblWindDirection: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupTopBorder(cvFrame: self.frame)
        self.layer.addSublayer(topBorder)
    }
    
    func updateCell() {
        if let data = weatherData {
            if let currentData = data.currentWeatherData {
                let sunrise = currentData.sunrise
                let sunset = currentData.sunset
                let sunraseDate = Date(timeIntervalSince1970: Double(sunrise))
                let sunsetDate = Date(timeIntervalSince1970: Double(sunset))
                lblSunrise.text = String(Localization.getLongTime(date: sunraseDate))
                lblSunset.text = String(Localization.getLongTime(date: sunsetDate))
            }
            
            if let firstHourFromForecast = data.firstNextHourFromFourlyForecast {
                lblClouds.text = String(firstHourFromForecast.clouds) + "%"
                lblHumidity.text = String(firstHourFromForecast.humidity) + "%"
                lblWind.text = String(firstHourFromForecast.windSpeed) + " m/s"
                lblWindDirection.text = String(firstHourFromForecast.windDeg) + "\u{00B0}"
                lblPressure.text = String(Int(firstHourFromForecast.pressure.rounded())) + " hPa"
            }
            
        }
    }
    
    func setupTopBorder(cvFrame: CGRect) {
        topBorder.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: borderWidth)
        topBorder.backgroundColor = UIColor.white.withAlphaComponent(0.6).cgColor
    }
}
