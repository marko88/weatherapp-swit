//
//  MenuFooterCell.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/1/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class MenuFooterCell: UITableViewCell {
    
    weak var menuVCDelegate: MenuViewControllerDelegate?
    
    @IBOutlet weak var btnCelsiusOutlet: UIButton!
    @IBOutlet weak var bntFahrenheitOutlet: UIButton!
    
    @IBAction func btnCelsiusAction(_ sender: UIButton) {
        hightligh(button: .Celsius)
        Settings.temperatureScale = .Celsius
        menuVCDelegate?.menuTebleViewControllerReloadData()
    }
    @IBAction func btnFahrenheitAction(_ sender: UIButton) {
        hightligh(button: .Fahrenheit)
        Settings.temperatureScale = .Fahrenheit
        menuVCDelegate?.menuTebleViewControllerReloadData()
    }
    
    private let textColorHighlited = UIColor.white.withAlphaComponent(1)
    private let textColorFaded = UIColor.white.withAlphaComponent(0.4)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clear
        btnCelsiusOutlet.titleLabel?.textColor = UIColor.red
        
        hightligh(button: Settings.temperatureScale)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func hightligh(button: Temp) {
        switch button {
        case .Celsius:
            btnCelsiusOutlet.setTitleColor(textColorHighlited, for: UIControlState.normal)
            bntFahrenheitOutlet.setTitleColor(textColorFaded, for: UIControlState.normal)
        case .Fahrenheit:
            btnCelsiusOutlet.setTitleColor(textColorFaded, for: UIControlState.normal)
            bntFahrenheitOutlet.setTitleColor(textColorHighlited, for: UIControlState.normal)
        default: break
        }
    }
}
