//
//  IOGrowHeader.swift
//  Test
//
//  Created by Marko Zec on 10/24/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit
import IOStickyHeader

class StickyHeaderCell: UICollectionViewCell {
    
    var weatherData: WeatherData?
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblWeatherDescription: UILabel!
    @IBOutlet weak var stackTemp: UIStackView!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var lblTempSymbol: UILabel!
    @IBOutlet weak var hourlyColletionView: ViewForHourlyCollectionView!
    @IBOutlet weak var hourlyCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var hourlyCollectionViewWidth: NSLayoutConstraint!
    @IBOutlet weak var distanceFromTop: NSLayoutConstraint!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var lblToday: UILabel!
    @IBOutlet weak var lblTempMin: UILabel!
    @IBOutlet weak var lblTempMax: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTempSymbol.text = "-"
        
        lblWeatherDescription.text = "----"
        lblToday.text = "----"
        lblTempMax.text = "--"
        lblTempMin.text = "--"
        
        cellView.backgroundColor = UIColor.clear
        
        backgroundImage.image = UIImage(named: "1")
        
        hourlyColletionView.viewHeight = hourlyCollectionViewHeight
        hourlyColletionView.viewWidth = self.frame.width
        
//        print("StickyHeaderCell:\(self.frame.width) hourlyColletionView:\(hourlyColletionView.frame.width)")
    }
    
    func updateCell() {
        if let data = weatherData {
            lblCity.text = data.name
            if let currentData = data.currentWeatherData {
                lblTempSymbol.text = "°"
                let weatherDescription = currentData.weatherDescription
                let capitalizedWeatherDescription = weatherDescription.capitalizingFirstLetterForAllWords()
                lblWeatherDescription.text = capitalizedWeatherDescription
                
                let convertedTemp = WeatherDataManager.convertTempTo(unit: Settings.temperatureScale, temp: currentData.temp)
                lblTemp.text = String(describing: convertedTemp)
                let convertedTempMax = WeatherDataManager.convertTempTo(unit: Settings.temperatureScale, temp: currentData.tempMax)
                lblTempMax.text = String(describing: convertedTempMax) + "\u{00B0}"
                let convertedTempMin = WeatherDataManager.convertTempTo(unit: Settings.temperatureScale, temp: currentData.tempMin)
                lblTempMin.text = String(describing: convertedTempMin) + "\u{00B0}"
                
                let date = Date(timeIntervalSince1970: Double(currentData.dt))
                lblToday.text = Localization.getLongDayName(date: date)
            }
            
            if data.hourlyForecastWeatherData.count > 0 {
                if let hourlyForecastData = data.hourlyForecastWeatherDataForNextMax20Hours {
                    hourlyColletionView.hourlyForecastWeatherData = hourlyForecastData
                    hourlyColletionView.updateData()
                }
            }
        }
    }
}
