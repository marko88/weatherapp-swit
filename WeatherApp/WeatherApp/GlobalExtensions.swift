//
//  GlobalExtensions.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/28/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func capitalizingFirstLetterForAllWords() -> String {
        let array = self.components(separatedBy: " ")
        var newArray = [String]()
        
        for word in array {
            newArray.append(word.capitalizingFirstLetter())
        }
        
        return newArray.joined(separator: " ")
    }
}
