//
//  ViewForHourlyCollection.swift
//  Test
//
//  Created by Marko Zec on 10/25/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class ViewForHourlyCollectionView: UIView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    var sortedHourlyForecastKeys = [Int]()
    var hourlyForecastWeatherData = [Int:OneHourWeatherData?]()
    
    var viewHeight: NSLayoutConstraint?
    var viewWidth: CGFloat?
    
    var collectionView: UICollectionView!
    
    let borderWidth = CGFloat(1)
    let buttomBorder = CALayer()
    let topBorder = CALayer()
    
    let cellNib = UINib(nibName: "ViewForHourlyCollectionCell", bundle: Bundle.main)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let newFrame = CGRect(x: 0, y: 0, width: viewWidth!, height: viewHeight!.constant)
        self.frame = newFrame
        
        self.setupCollectionView(cvFrame: self.frame)
        self.setupTopBorder(cvFrame: self.frame)
        self.setupBottomBorder(cvFrame: self.frame)
        
        self.addSubview(collectionView)
        self.layer.addSublayer(topBorder)
        self.layer.addSublayer(buttomBorder)
        
//        print("ViewForHourlyCollection:\(self.frame.width) collection:\(collectionView.frame.width)")
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        collectionView?.reloadData()
    }
    
    func updateData() {
        sortedHourlyForecastKeys = hourlyForecastWeatherData.keys.sorted()
    }
    
    func setupBottomBorder(cvFrame: CGRect) {
        buttomBorder.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: borderWidth)
        buttomBorder.backgroundColor = UIColor.white.withAlphaComponent(0.6).cgColor
    }
    
    func setupTopBorder(cvFrame: CGRect) {
        topBorder.frame = CGRect(x: 0, y: self.frame.height-borderWidth, width: self.frame.width, height: borderWidth)
        topBorder.backgroundColor = UIColor.white.withAlphaComponent(0.6).cgColor
    }
    
    func setupCollectionView(cvFrame: CGRect) {
        collectionView = UICollectionView(frame: cvFrame, collectionViewLayout: setupCollectionViewLayout(cvFrame: cvFrame))
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(cellNib, forCellWithReuseIdentifier: "hourlyCell")
        
        collectionView.backgroundColor = UIColor.clear // clear is transparent
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPrefetchingEnabled = true
        collectionView.isScrollEnabled = true
        collectionView.scrollsToTop = true
        collectionView.isUserInteractionEnabled = true
        collectionView.isMultipleTouchEnabled = true
        collectionView.autoresizesSubviews = true
        collectionView.clipsToBounds = true
        collectionView.clearsContextBeforeDrawing = true
        collectionView.isOpaque = true
        collectionView.allowsSelection = false
        collectionView.isPagingEnabled = false
    }
    
    func setupCollectionViewLayout(cvFrame: CGRect) -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: cvFrame.height*2/3, height: cvFrame.height)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        return layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hourlyForecastWeatherData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hourlyCell", for: indexPath) as! ViewForHourlyCollectionCell
        
        if let data = hourlyForecastWeatherData[sortedHourlyForecastKeys[indexPath.row]] {
            cell.oneHourWeatherData = data
            cell.dt = sortedHourlyForecastKeys[indexPath.row]
        }
        
        cell.paretFrame = self.frame
        cell.updateCell()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected cell \(indexPath.row)")
    }
    
}
