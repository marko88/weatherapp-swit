//
//  MenuTableCell.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/31/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
        
    var elementIndex = 1
    
    var commonData: CommonWeatherData?
    var currentData: CurrentHourWeatherData?

    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBOutlet weak var btnInfoOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = UIColor.clear
        self.btnInfoOutlet.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell() {
        var title: String
        var city = "--"
        var countryCode = "--"
        
        if let data = commonData {
            city = data.name
            countryCode = data.country
        }
        
        if let tempInKelvin = currentData?.temp {
            let temp = WeatherDataManager.convertTempTo(unit: Settings.temperatureScale, temp: tempInKelvin)
            title = "\(String(format: "%02d", temp))\u{00B0}  \(city), \(countryCode)"
        } else {
            title = "--  \(city), \(countryCode)"
        }
        
        btnInfoOutlet.setTitle(title, for: UIControlState.normal)
    }
}

