//
//  PageViewController.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/26/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class PagesVC: UIPageViewController {
    
    weak var pagesVCDelegate: PagesVCDelegate?

    var currentVCIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        self.setViewControllers([orderedViewControllers[currentVCIndex]], direction: .forward, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.reloadInputViews()
        
        pagesVCDelegate?.pagesViewController(pagesViewController: self, didUpdatePageCount: WeatherDataManager.getLocationsID().count)
        pagesVCDelegate?.pagesViewController(pagesViewController: self, didUpdatePageIndex: currentVCIndex)
        setViewControllers([orderedViewControllers[currentVCIndex]], direction: .forward, animated: true, completion: nil)
    }
    
    private(set) lazy var orderedViewControllers: [WeatherVC] = {
        var array = [WeatherVC]()
        
        for id in WeatherDataManager.getLocationsID() {
            if let data = WeatherDataManager.getLocations()[id] {
                array.append(self.newWeatherVC(weatherData: data))
            }
        }
        
        return array
    }()
    
    private func newWeatherVC(weatherData: WeatherData) -> WeatherVC {
        let contoller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WeatherVC") as! WeatherVC
        contoller.weatherData = weatherData
        return contoller
    }
}

// MARK: UIPageViewControllerDataSource

extension PagesVC: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController as! WeatherVC) else {
            return nil
        }
        
        currentVCIndex = viewControllerIndex
        
        let previousIndex = viewControllerIndex - 1
        
        guard orderedViewControllers.indices.contains(previousIndex) else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController as! WeatherVC) else {
            return nil
        }
        
        currentVCIndex = viewControllerIndex
        
        let nextIndex = viewControllerIndex + 1
        
        guard orderedViewControllers.indices.contains(nextIndex) else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}

// MARK: UIPageViewControllerDelegate

extension PagesVC: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.index(of: firstViewController as! WeatherVC) {
            pagesVCDelegate?.pagesViewController(pagesViewController: self, didUpdatePageIndex: index)
        }
    }
}
