//
//  PagesVCDataSource.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/26/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation

protocol PagesVCDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter count: the total number of pages.
     */
    func pagesViewController(pagesViewController: PagesVC, didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func pagesViewController(pagesViewController: PagesVC, didUpdatePageIndex index: Int)
    
}
