//
//  WeatherHeaderCell.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/27/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class WeatherHeaderCell: UICollectionViewCell {
    
    var dayInterval = 0
    var tempMax = 0.0
    var tempMin = 0.0
    var weatherIconName = "icon-number"
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblTempMax: UILabel!
    @IBOutlet weak var lblTempMin: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCell() {
        
        let date = Date(timeIntervalSince1970: Double(dayInterval))
        
        let tempMax = WeatherDataManager.convertTempTo(unit: Settings.temperatureScale, temp: self.tempMax)
        let tempMin = WeatherDataManager.convertTempTo(unit: Settings.temperatureScale, temp: self.tempMin)
        
        lblDay.text = Localization.getLongDayName(date: date)
        lblTempMax.text = "\(tempMax)\u{00B0}"
        lblTempMin.text = "\(tempMin)\u{00B0}"
        weatherIcon.image = UIImage(named: weatherIconName)
    }
}
