//
//  ListOfOneDayWeatherData.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/3/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListOfOneDayWeatherData {
    private(set) var parsingSucceeded = true
    
    private(set) var list = [Int:OneDayWeatherData?]()
    
    init(data: JSON) {
        let dataList = data["list"]
        
        for index in 0..<dataList.count {
            var dt = 0 // unix time
            
            dt = dataList[index]["dt"].int ?? defaultIntValue()
            
            let oneDayData = OneDayWeatherData(data: dataList[index])
            if oneDayData.parsingSucceeded {
                list[dt] = oneDayData
            } else { parsingSucceeded = false }
        }
    }
    
    private func defaultIntValue() -> Int {
        parsingSucceeded = false
        return 0
    }
}


