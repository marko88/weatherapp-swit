//
//  Localizations.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/3/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation

class Localization {
    
    static func getShortDayName(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.setLocalizedDateFormatFromTemplate("EEE")
        let formattedDate = dateFormatter.string(from: date)
        
        return formattedDate
    }
    
    static func getLongDayName(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.setLocalizedDateFormatFromTemplate("EEEE")
        let formattedDate = dateFormatter.string(from: date)
        
        return formattedDate
    }
    
    static func getShortTime(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.setLocalizedDateFormatFromTemplate("h")
        let formattedDate = dateFormatter.string(from: date)
        
        return formattedDate
    }
    
    static func getLongTime(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.setLocalizedDateFormatFromTemplate("h:mm a")
        let formattedDate = dateFormatter.string(from: date)
        
        return formattedDate
    }
}
