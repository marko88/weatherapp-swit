//
//  CurrentHourWeatherData.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/3/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import SwiftyJSON

class CurrentHourWeatherData {
    private(set) var parsingSucceeded = true
    
    private(set) var temp: Double = 0.0
    private(set) var tempMin: Double = 0.0
    private(set) var tempMax: Double = 0.0
    
    private(set) var weatherID: Int = 0
    private(set) var weatherType: String = ""
    private(set) var weatherDescription: String = ""
    private(set) var weatherIcon: String = ""
    
    private(set) var dt: Int = 0
    private(set) var sunrise: Int = 0
    private(set) var sunset: Int = 0
    
    init(data: JSON) {
        let main = data["main"]
        let weather = data["weather"][0]
        
        dt = data["dt"].int ?? defaultIntValue()
        sunrise = data["sys"]["sunrise"].int ?? defaultIntValue()
        sunset = data["sys"]["sunset"].int ?? defaultIntValue()
        temp = main["temp"].double ?? defaultDoubleValue()
        tempMin = main["temp_min"].double ?? defaultDoubleValue()
        tempMax = main["temp_max"].double ?? defaultDoubleValue()
        weatherID = weather["id"].int ?? defaultIntValue()
        weatherType = weather["main"].string ?? defaultStringValue()
        weatherDescription = weather["description"].string ?? defaultStringValue()
        weatherIcon = weather["icon"].string ?? defaultStringValue()
        
    }
    
    private func defaultDoubleValue() -> Double {
        parsingSucceeded = false
        return 0.0
    }
    
    private func defaultIntValue() -> Int {
        parsingSucceeded = false
        return 0
    }
    
    private func defaultStringValue() -> String {
        parsingSucceeded = false
        return ""
    }
    
    init(oneHourData: OneHourWeatherData, oldCurrentData: CurrentHourWeatherData) {
        self.sunset = oldCurrentData.sunset
        self.sunrise = oldCurrentData.sunrise
        
        self.temp = oneHourData.temp
        self.tempMin = oneHourData.tempMin
        self.tempMax = oneHourData.tempMax
        
        self.weatherID = oneHourData.weatherID
        self.weatherType = oneHourData.weatherType
        self.weatherDescription = oneHourData.weatherDescription
        self.weatherIcon = oneHourData.weatherIcon
        
        self.dt = oneHourData.dt
    }
}
