//
//  MenuTableViewControllerDelegate.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/31/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation

protocol MenuViewControllerDelegate: class {
//    func menuTebleViewController(cell: MenuCell, deleteLocationWithIndex: Int) -> Void
    func menuTebleViewControllerReloadData() -> Void
}
