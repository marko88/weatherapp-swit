//
//  OneDayWeatherData.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/3/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import SwiftyJSON

class OneDayWeatherData {
    private(set) var parsingSucceeded = true
    
    private(set) var tempDay: Double = 0.0
    private(set) var tempMin: Double = 0.0
    private(set) var tempMax: Double = 0.0
    private(set) var tempNight: Double = 0.0
    private(set) var tempEvening: Double = 0.0
    private(set) var tempMorning: Double = 0.0
    private(set) var pressure: Double = 0.0
    private(set) var humidity: Int = 0
    
    private(set) var windSpeed: Double = 0.0
    private(set) var windDeg: Int = 0
    private(set) var clouds: Int = 0
    
    private(set) var weatherID: Int = 0
    private(set) var weatherType: String = ""
    private(set) var weatherDescription: String = ""
    private(set) var weatherIcon: String = ""
    
    init(data: JSON) {
        let temp = data["temp"]
        let weather = data["weather"][0]
        
        // Temperature
        
        tempDay = temp["day"].double ?? defaultDoubleValue()
        tempMin = temp["min"].double ?? defaultDoubleValue()
        tempMax = temp["max"].double ?? defaultDoubleValue()
        tempNight = temp["night"].double ?? defaultDoubleValue()
        tempEvening = temp["eve"].double ?? defaultDoubleValue()
        tempMorning = temp["morn"].double ?? defaultDoubleValue()
        
        // Other
        
        pressure = data["pressure"].double ?? defaultDoubleValue()
        humidity = data["humidity"].int ?? defaultIntValue()
        windSpeed = data["speed"].double ?? defaultDoubleValue()
        windDeg = data["deg"].int ?? defaultIntValue()
        clouds = data["clouds"].int ?? defaultIntValue()
        
        
        // Weather
        
        weatherID = weather["id"].int ?? defaultIntValue()
        weatherType = weather["main"].string ?? defaultStringValue()
        weatherDescription = weather["description"].string ?? defaultStringValue()
        weatherIcon = weather["icon"].string ?? defaultStringValue()
        
    }
    
    private func defaultDoubleValue() -> Double {
        parsingSucceeded = false
        return 0.0
    }
    
    private func defaultIntValue() -> Int {
        parsingSucceeded = false
        return 0
    }
    
    private func defaultStringValue() -> String {
        parsingSucceeded = false
        return ""
    }
}
