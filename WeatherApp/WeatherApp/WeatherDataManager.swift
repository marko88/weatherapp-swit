//
//  WeatherData.swift
//  UIPageViewControllerDemo2
//
//  Created by Marko Zec on 10/23/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import CoreLocation
import ReachabilitySwift

class WeatherDataManager: NSObject, CLLocationManagerDelegate {
    
    private static let sharedInstance = WeatherDataManager()
    
    static let notificationKeyForCurrentLocation = "weatherapp.curentlocation"
    private let reachabilitySwift = Reachability()
    
    private var chosenLocationsID = [Int]()
    private var chosenLocationsData = [Int:WeatherData]()
    
    private var listOfAdapters = [ProgressBarDelegate?]()
    
    private var TEMORARY_LIST_OF_CITIES: [(id:Int, name:String, country:String)]?
    private var IS_LIST_OF_CITIES_LOADED = false
    
    let locationManager = CLLocationManager()
    private var lastLocationUpdateTime = 0.0
    private let updateIntervalInSeconds = 3600.0 // updates hourly
    
    override private init() {
        super.init()
        // Reserve first element for Current Location
        chosenLocationsID.append(0)
        chosenLocationsData[0] = WeatherData(usesLocation: true)
        startUpdatingLocation()
        
        // Supported locations from local files:
        //   792680  Belgrade, RS
        //   2643743 London, GB
        //   787657  Nis, RS
        //   6455259 Paris, FR
        //   524901  Moscow, RU
        //   3169070 Roma, IT
        
        _ = addLocation(id: 2643743) // London
        _ = addLocation(id: 792680) // Belgrade
        _ = addLocation(id: 6455259) // Paris
        _ = addLocation(id: 524901) // Moscow
        _ = addLocation(id: 3169070) // Roma
        _ = addLocation(id: 787657) // Nis
        
        // Locading list of cities in backgroud, this should be managed by core data
        getListOfCitiesAsync { list in
            if list != nil {
                print("List of Cities is loaded")
                self.TEMORARY_LIST_OF_CITIES = list
                self.IS_LIST_OF_CITIES_LOADED = true
                
                for element in self.listOfAdapters {
                    if let observer = element {
                        observer.progressionIsFinished()
                    }
                }
                
            } else {
                print("List of Cities faled to load")
            }
        }
    }
    
    private func startUpdatingLocation() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let reachability = reachabilitySwift {
            // If have internet access
            if reachability.isReachable {
                let currentTime = Date().timeIntervalSince1970
                if lastLocationUpdateTime + updateIntervalInSeconds < currentTime {
                    lastLocationUpdateTime = currentTime
                    let locValue = manager.location!.coordinate
                    print("locations = \(locValue.latitude) \(locValue.longitude) unix \(currentTime)")
                    
                    chosenLocationsData[0]?.setLocation(location: locValue)
                    chosenLocationsData[0]?.setDataForCurrentLocation()
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: WeatherDataManager.notificationKeyForCurrentLocation), object: self)
                }
            }
        }
    }
    
    private func addLocation(id: Int) -> Bool {
        if !chosenLocationsID.contains(id) {
            if addData(forID: id){
                chosenLocationsID.append(id)
                let data = chosenLocationsData[id]
                print("Location ADDED: \(data!.commonWeatherData!.name),\(data!.commonWeatherData!.country) - \(id)")
                return true
            }
        }
        
        return false
    }
    
    private func addData(forID: Int) -> Bool {
        if chosenLocationsData[forID] == nil {
            let weatherData = WeatherData(getDataForLoactionID: forID)
            if weatherData.parsingSucceeded {
                chosenLocationsData[forID] = weatherData
                return true
            }
        }
        
        return false
    }
    
    private func removeLocation(id :Int) -> Bool {
        if let index = chosenLocationsID.index(of: id) {
            if chosenLocationsData[id] != nil {
                chosenLocationsID.remove(at: index)
                chosenLocationsData.removeValue(forKey: id)
                return true
            }
        }
        
        return false
    }
    
    private func removeLocation(atIndex: Int) -> Bool {
        if chosenLocationsID.indices.contains(atIndex) {
            return removeLocation(id: chosenLocationsID[atIndex])
        }
        
        return false
    }
    
    private func getListOfCitiesAsync(hander: @escaping ([(id:Int, name:String, country:String)]?) -> Void ) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            
            func updateProgress(percent: Double) {
                DispatchQueue.main.async {
                    for element in self.listOfAdapters {
                        if let observer = element {
                            observer.updateProgress(progress: percent)
                        }
                    }
                }
            }
            
            if let list = JSONFromOWM.getCitiesFromLocalFile(updateProgress: updateProgress) {
                DispatchQueue.main.async { // if not in the main queue, it will take forever to hide the loadingView in SearchVC
                    hander(list)
                }
            } else {
                DispatchQueue.main.async {
                    hander(nil)
                }
            }
        }
    }
    
    
    // Public methods
    
    static func searchForLocationsContainig(searchText: String) -> [(id:Int, name:String, country:String)]? {
        print("Search for: \(searchText)")
        if let list = sharedInstance.TEMORARY_LIST_OF_CITIES {
            let filteredList = list.filter { (location) -> Bool in
                return location.name.lowercased().contains(searchText.lowercased())
            }
            
            // Simple stupid sort :)
            // Some kind of sorting must be implemented, otherwise short named cities like "Nis" can't be found in the search result
            var identical = [(id:Int, name:String, country:String)]()
            var beginingWith = [(id:Int, name:String, country:String)]()
            var contains = [(id:Int, name:String, country:String)]()
            
            for element in filteredList {
                if element.name.lowercased() == searchText.lowercased() {
                    identical.append(element)
                } else if element.name.lowercased().hasPrefix(searchText.lowercased()) {
                    beginingWith.append(element)
                } else {
                    contains.append(element)
                }
            }
            
            return identical + beginingWith + contains
        }
        
        return nil
    }
    
    static func isListForCitiesLoaded() -> Bool {
        return sharedInstance.IS_LIST_OF_CITIES_LOADED
    }
    
    static func getListOfCities() -> [(id:Int, name:String, country:String)]? {
        return sharedInstance.TEMORARY_LIST_OF_CITIES
    }
    
    static func addLocation(id: Int) -> Bool {
        return sharedInstance.addLocation(id: id)
    }
    
    static func removeLastAdapter() {
        sharedInstance.listOfAdapters.removeLast()
    }
    
    static func addAdapter(adapter: ProgressBarDelegate?) {
        sharedInstance.listOfAdapters.append(adapter)
    }
    
    static func removeLocation(atIndex: Int) -> Bool {
        return sharedInstance.removeLocation(atIndex: atIndex)
    }
    
    static func getLocationsID() -> [Int] {
        return sharedInstance.chosenLocationsID
    }
    
    static func getLocations() -> [Int:WeatherData] {
        return sharedInstance.chosenLocationsData
    }
    
    static func convertTempTo(unit: Temp, temp: Double) -> Int {
        var newTemp = 0
        
        switch unit {
        case .Celsius:
            newTemp = Int(round(temp - 273.15))
        case .Fahrenheit:
            newTemp = Int(round(temp * 9/5 - 459.67))
        case .Kelvin:
            newTemp = Int(round(temp))
        }
        
        return newTemp
    }
    
}

enum WeatherDataType {
    case Current
    case HourlyForecast
    case DailyForecast
}

enum Temp {
    case Celsius
    case Kelvin
    case Fahrenheit
}
