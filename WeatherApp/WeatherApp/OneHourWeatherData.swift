//
//  OneHourWeatherData.swift
//  UIPageViewControllerDemo2
//
//  Created by Marko Zec on 10/23/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import SwiftyJSON

class OneHourWeatherData {
    private(set) var parsingSucceeded = true
    
    private(set) var temp: Double = 0.0
    private(set) var tempMin: Double = 0.0
    private(set) var tempMax: Double = 0.0
    
    private(set) var weatherID: Int = 0
    private(set) var weatherType: String = ""
    private(set) var weatherDescription: String = ""
    private(set) var weatherIcon: String = ""
    
    private(set) var pressure: Double = 0.0
    private(set) var humidity: Int = 0
    private(set) var windSpeed: Double = 0.0
    private(set) var windDeg: Int = 0
    private(set) var clouds: Int = 0
    
    private(set) var dt: Int = 0
    
    init(data: JSON) {
        let main = data["main"]
        let weather = data["weather"][0]
        
        // Temperature
        
        temp = main["temp"].double ?? defaultDoubleValue()
        tempMin = main["temp_min"].double ?? defaultDoubleValue()
        tempMax = main["temp_max"].double ?? defaultDoubleValue()
        
        // Weather
        
        weatherID = weather["id"].int ?? defaultIntValue()
        weatherType = weather["main"].string ?? defaultStringValue()
        weatherDescription = weather["description"].string ?? defaultStringValue()
        weatherIcon = weather["icon"].string ?? defaultStringValue()
        
        // Other
        
        dt = data["dt"].int ?? defaultIntValue()
        pressure = main["pressure"].double ?? defaultDoubleValue()
        humidity = main["humidity"].int ?? defaultIntValue()
        windSpeed = data["wind"]["speed"].double ?? defaultDoubleValue()
        windDeg = data["wind"]["deg"].int ?? defaultIntValue()
        clouds = data["clouds"]["all"].int ?? defaultIntValue()
        
    }
    
    private func defaultDoubleValue() -> Double {
        parsingSucceeded = false
        return 0.0
    }
    
    private func defaultIntValue() -> Int {
        parsingSucceeded = false
        return 0
    }
    
    private func defaultStringValue() -> String {
        parsingSucceeded = false
        return ""
    }
}

