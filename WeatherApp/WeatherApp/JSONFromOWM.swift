//
//  JSONFromOWM.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/28/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

class JSONFromOWM {
    static let sourceIsLocalFileForTesting = true
    static let appID = "41206e6cab9325d6234a81c50b75abc1"
    
    static func getWeather(forecastType: WeatherDataType,location: CLLocationCoordinate2D) -> JSON? {
        let jsonString = getJSONString(location: location, forecastType: forecastType)
        return getWeatherJSON(jsonString: jsonString)
    }
    
    static func getWeather(forecastType: WeatherDataType, cityID id: Int) -> JSON? {
        let jsonString = getJSONString(cityID: id, forecastType: forecastType)
        return getWeatherJSON(jsonString: jsonString)
    }
    
    private static func getJSONString(location: CLLocationCoordinate2D, forecastType: WeatherDataType) -> NSString? {
        return JSONStringFromOWM(location: location, forecastType: forecastType)
    }
    
    private static func getJSONString(cityID id: Int, forecastType: WeatherDataType) -> NSString? {
        var jsonString: NSString?
        
        if sourceIsLocalFileForTesting {
            jsonString = JSONStringFromLocalFile(cityID: id, forecastType: forecastType)
        } else {
            jsonString = JSONStringFromOWM(cityID: id, forecastType: forecastType)
        }
        
        return jsonString
    }
    
    private static func getWeatherJSON(jsonString: NSString?) -> JSON? {
        if let json = jsonString {
            if let data = json.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false) {
                return JSON(data: data)
            }
        } else {
            print("Error: Some error occurred while obtaining JSON")
        }
        
        return nil
    }
    
    // This func must be called async, it will take more than 1 min
    static func getCitiesFromLocalFile(updateProgress: (_ progress: Double) -> Void) -> [(id:Int, name:String, country:String)]? {
        var array = [(id:Int, name:String, country:String)]()
        
        var arrayOfJSONs = [String]()
        
        let fileName = "city.list"
        let type = "json"
        
        if let string = getFileContent(fileName: fileName, type: type) {
            arrayOfJSONs = string.components(separatedBy: "\n")
            
            if arrayOfJSONs.count > 0 {
                var counter = 0
                var percent = 0
                let onePercent = arrayOfJSONs.count / 100
                
                updateProgress(Double(0))
                for line in arrayOfJSONs {
                    
                    if percent < counter / onePercent {
                        percent += 1
                        updateProgress(Double(percent))
                    }
                    
                    if let data = (line as NSString).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false) {
                        var json = JSON(data: data)
                        array.append(
                            (
                                id: json["_id"].intValue,
                                name: json["name"].stringValue,
                                country: json["country"].stringValue
                            )
                        )
                    } else {
                        print("Error: Encoding faled for \(fileName).\(type)")
                    }
                    
                    counter += 1
                }
                
            }
        } else {
            print("Error: Some error occurred while obtaining JSON from \(fileName).\(type)")
        }
        
        if array.count > 0 {
            return array
        }
        
        return nil
    }
    
    private static func JSONStringFromLocalFile(cityID id: Int, forecastType: WeatherDataType) -> NSString? {
        var fileName: String
        let type = "json"
        
        switch forecastType {
        case .Current:
            fileName = "currentWeather-\(id)"
            
        case .HourlyForecast:
            fileName = "hourlyForecastWeather-\(id)"
            
        case .DailyForecast:
            fileName = "dailyForecastWeather-\(id)"
        }
        
        return getFileContent(fileName: fileName, type: type)
    }
    
    private static func getFileContent(fileName:String, type: String) -> NSString? {
        var string: NSString?
        
        if let path = Bundle.main.path(forResource: fileName, ofType: type) {
            do {
                string = try NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue)
            } catch {
                print("Error: Can't open file \(fileName).\(type)")
                print(error)
            }
        } else {
            print("Error: Can't get a path for file:\(fileName) of type:\(type)")
        }
        
        return string
    }
    
    private static func JSONStringFromOWM(cityID id: Int, forecastType: WeatherDataType) -> NSString? {
        return JSONStringFromOWMWith(parametersForGET: "id=\(id)", forecastType: forecastType)
    }
    
    private static func JSONStringFromOWM(location: CLLocationCoordinate2D, forecastType: WeatherDataType) -> NSString? {
        return JSONStringFromOWMWith(parametersForGET: "lat=\(location.latitude)&lon=\(location.longitude)", forecastType: forecastType)
    }
    
    private static func JSONStringFromOWMWith(parametersForGET: String, forecastType: WeatherDataType) -> NSString? {
        var jsonString: NSString?
        var urlString: String
        
        switch forecastType {
        case .Current:
            urlString = "http://api.openweathermap.org/data/2.5/weather?\(parametersForGET)&appid=\(JSONFromOWM.appID)"
            
        case .HourlyForecast:
            urlString = "http://api.openweathermap.org/data/2.5/forecast?\(parametersForGET)&cnt=36&appid=\(JSONFromOWM.appID)"
            
        case .DailyForecast:
            urlString = "http://api.openweathermap.org/data/2.5/forecast/daily?\(parametersForGET)&cnt=16&appid=\(JSONFromOWM.appID)"
        }
        
        if let url = NSURL(string: urlString) {
            do {
                jsonString = try NSString(contentsOf: url as URL, encoding: String.Encoding.utf8.rawValue)
            } catch {
                print("Error: Can't get resource for \(urlString)")
                print(error)
            }
        } else {
            print("Error: Can't use URL of \(urlString)")
        }
        
        return jsonString
    }
    
}

