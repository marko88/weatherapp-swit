//
//  SearchTableViewCell.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/1/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    var isActive = true
    var id: Int?
    var name: String?
    var country: String?
    
    let alphaForActive: CGFloat = 1.0
    let alphaForInactive: CGFloat = 0.5
    
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.clear
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateCell() {
        
        if isActive {
            label.textColor = label.textColor.withAlphaComponent(alphaForActive)
        } else {
            label.textColor = label.textColor.withAlphaComponent(alphaForInactive)
        }
        
        if  let _name = name,
            let _countru = country
        {
            label.text = "\(_name), \(_countru)"
        }
        
    }
}
