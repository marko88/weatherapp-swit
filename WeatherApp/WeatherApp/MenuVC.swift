//
//  MenuTableVC.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/31/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit
import CoreLocation

class MenuVC: UIViewController, MenuViewControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        if let caontainerVC = storyboard?.instantiateViewController(withIdentifier: "containerVC") as? ContainerVC {
            caontainerVC.startingLoactionIndex = Settings.lastSelectedPageIndex
            self.navigationController?.pushViewController(caontainerVC, animated: false)
        }
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func menuTebleViewControllerReloadData() {
        tableView.reloadData()
    }
    
}

extension MenuVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return WeatherDataManager.getLocationsID().count + 1 // +1 is footer nib
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        if indexPath.row < WeatherDataManager.getLocationsID().count {
            if let menuCell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as? MenuCell {
                
                let locationID = WeatherDataManager.getLocationsID()[indexPath.row]
                if let location = WeatherDataManager.getLocations()[locationID] {
                    menuCell.elementIndex = indexPath.row
                    menuCell.commonData = location.commonWeatherData
                    menuCell.currentData = location.currentWeatherData
                }
                
                menuCell.updateCell()
                
                cell = menuCell
            }
        
        } else {
            if let footerCell = tableView.dequeueReusableCell(withIdentifier: "footerMenuCell", for: indexPath) as? MenuFooterCell {
                footerCell.menuVCDelegate = self
                cell = footerCell
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 {
            return false
        }
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            if WeatherDataManager.removeLocation(atIndex: indexPath.row) {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            } else {
                let index = WeatherDataManager.getLocationsID()[indexPath.row]
                let location = WeatherDataManager.getLocations()[index]?.commonWeatherData?.name ?? "Location"
                self.alertErrorRemoveLocation(location: location)
            }
        }
    }
    
    private func alertErrorRemoveLocation(location: String) {
        let title = "Error"
        let message = "Error occures while removing \(location) from the list."
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension MenuVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let containerVC = storyboard?.instantiateViewController(withIdentifier: "containerVC") as? ContainerVC,
            indexPath.row < WeatherDataManager.getLocationsID().count
        {
            containerVC.startingLoactionIndex = indexPath.row
            self.navigationController?.pushViewController(containerVC, animated: true)
        }
    }
    
}
