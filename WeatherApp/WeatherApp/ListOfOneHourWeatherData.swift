//
//  ListOfOneHourWeatherData.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/28/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListOfOneHourWeatherData {
    private(set) var parsingSucceeded = true
    
    private(set) var list = [Int:OneHourWeatherData?]()
    
    init(data: JSON) {
        let dataList = data["list"]
        
        for index in 0..<dataList.count {
            var dt = 0 // unix time
            
            dt = dataList[index]["dt"].int ?? defaultIntValue()
            
            let oneHourData = OneHourWeatherData(data: dataList[index])
            if oneHourData.parsingSucceeded {
                list[dt] = oneHourData
            } else { parsingSucceeded = false }
        }
    }
    
    private func defaultIntValue() -> Int {
        parsingSucceeded = false
        return 0
    }
}

