//
//  ViewController.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/26/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {

    @IBOutlet weak var backgorundImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnOWM: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBAction func btnMenuAction(_ sender: UIButton) {
        self.navigationController!.popViewController(animated: true)
    }
    
    var startingLoactionIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgorundImage.image = UIImage(named: "1")
        pageControl.defersCurrentPageDisplay = true // disables click on page control, for now
        
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1)
        topBorder.backgroundColor = UIColor.white.withAlphaComponent(0.6).cgColor
        bottomView.layer.addSublayer(topBorder)
        
        pageControl.currentPage = startingLoactionIndex
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let pagesVC = segue.destination as? PagesVC {
            pagesVC.currentVCIndex = startingLoactionIndex
            pagesVC.pagesVCDelegate = self
        }
    }
}

extension ContainerVC: PagesVCDelegate {
    
    func pagesViewController(pagesViewController: PagesVC, didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func pagesViewController(pagesViewController: PagesVC, didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    
}

