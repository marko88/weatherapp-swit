//
//  SearchVC.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/1/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class SearchVC: UIViewController, ProgressBarDelegate {

    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var progessView: UIProgressView!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var filteringIndicator: UIActivityIndicatorView!
    @IBAction func btnBack(_ sender: UIButton) {
        if let navigation = self.navigationController {
            navigation.popViewController(animated: true)
            WeatherDataManager.removeLastAdapter() // remove the reference from singleton
        }
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var filteredResult = [(id:Int, name:String, country:String)]()
    var typingTimer = Timer()
    var isCellsActive = true
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filteringIndicator.stopAnimating()
        
        if WeatherDataManager.isListForCitiesLoaded() {
            showSearch()
        } else {
            showLoading()
            self.progessView.setProgress(Float(0), animated: false)
            WeatherDataManager.addAdapter(adapter: self)
            
        }
        
        setupTableView()
        setupSearchController()
    }
    
    private func setupTableView() {
        tableView.backgroundColor = UIColor.clear
    }

    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.showsCancelButton = true
        searchController.searchBar.delegate = self
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        let text = searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if text != "" {
            typingTimer.invalidate()
            typingTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (Timer) in
                DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                    DispatchQueue.main.async {
                        self.reloadInactiveCells()
                    }
                    if let searchResult = WeatherDataManager.searchForLocationsContainig(searchText: text) {
                        DispatchQueue.main.async {
                            self.filteredResult = searchResult
                            print("RELOADING DATA:")
                            self.reloadActiveCells()
                        }
                    }
                    
                }
            }
        } else {
            filteredResult.removeAll()
            self.tableView.reloadData()
        }
    }
    
    private func reloadActiveCells() {
        isCellsActive = true
        tableView.reloadData()
        self.filteringIndicator.stopAnimating()
    }
    
    private func reloadInactiveCells() {
        isCellsActive = false
        tableView.reloadData()
        self.filteringIndicator.startAnimating()
    }
    
    func updateProgress(progress: Double) {
        self.progessView.setProgress(Float(progress/100), animated: true)
        self.lblProgress.text = "\(Int(progress)) %"
    }
    
    func progressionIsFinished() {
        self.showSearch()
    }
    
    private func showSearch() {
        self.loadingView.isHidden = true
        self.searchView.isHidden = false
    }
    
    private func showLoading() {
        self.loadingView.isHidden = false
        self.searchView.isHidden = true
    }

}

extension SearchVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
}

extension SearchVC: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        self.navigationController!.popViewController(animated: true)
    }
}

extension SearchVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            print("Count filtered \(filteredResult.count)")
            return filteredResult.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchTableViewCell", for: indexPath) as! SearchTableViewCell
        
        let element = filteredResult[indexPath.row]
        cell.id = element.id
        cell.name = element.name
        cell.country = element.country
        cell.isActive = isCellsActive
        
        if WeatherDataManager.getLocationsID().contains(element.id) {
            cell.isActive = false
        }
        
        print("\(cell.name!) - \(cell.id!) indexPath(\(indexPath.count),\(indexPath.row))")
        
        cell.updateCell()
        
        return cell
    }
}

extension SearchVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let id = filteredResult[indexPath.row].id
        
        // Selected cell is in the list
        if !isCellsActive || WeatherDataManager.getLocationsID().contains(id) {
            return
        }
        
        let name = filteredResult[indexPath.row].name
        let country = filteredResult[indexPath.row].country
        
        print("Selected \(name)-\(id) indexPath(\(indexPath.count),\(indexPath.row))")
        
        if WeatherDataManager.addLocation(id: id) {
            self.navigationController!.popViewController(animated: true)
        }  else {
            alertLocationAddingFaled(city: name, country: country)
        }
    }
    
    private func alertLocationAddingFaled(city: String, country: String) {
        let title = "Error"
        let message = "Sorry some error occured, \(city), \(country) can not be added."
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
