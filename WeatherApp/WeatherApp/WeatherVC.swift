//
//  WeatherVC.swift
//  WeatherApp
//
//  Created by Marko Zec on 10/26/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit
import IOStickyHeader
import CoreLocation
import CoreGraphics

class WeatherVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorViewText: UILabel!
    @IBOutlet weak var errorViewButon: UIButton!
    
    @IBAction func errorViewButton(_ sender: UIButton) {
        if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
    }
    
    let textRestricted = "Current location restricted by parental controls."
    let textDenied = "Current weather is unavailable, please change permissions."
    
    let locationManager = CLLocationManager()
    
    var weatherData: WeatherData! {
        didSet {
            setDailyForecastForNext10Days()
        }
    }
    var next10Days = [Int:OneDayWeatherData]()
    var sortedKeysForNext10Days = [Int]()
    
    var stickyHeaderCell: StickyHeaderCell?
    let screenSize: CGRect = UIScreen.main.bounds
    
    var stickyHeaderMaxHeight: CGFloat = 700
    var stickyHeaderMinHeight: CGFloat = 230
    
    let headerNib = UINib(nibName: "StickyHeaderCell", bundle: Bundle.main)
    
    deinit {
        if weatherData.usesLocation {
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showView(type: .Weather)
        
        let screenHeight = screenSize.height
        stickyHeaderMaxHeight = screenHeight * 0.65
        stickyHeaderMinHeight = screenHeight * 0.35
        
        collectionView.backgroundColor = UIColor.clear
        
        self.setupCollectionView()
        
        if weatherData.usesLocation {
            locationManager.delegate = self
            NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: NSNotification.Name(rawValue: WeatherDataManager.notificationKeyForCurrentLocation), object: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if weatherData.usesLocation {
            collectionView.reloadData()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse,
             .authorizedAlways:
            showView(type: .Weather)
            break
        case .restricted:
            print("Access restricted")
            showView(type: .ErrorRestricted)
            break
        case .denied:
            print("Access denied")
            showView(type: .ErrorDenied)
            break
        }
    }
    
    @objc private func updateData() {
        print("Current Location is updated")
            
        if let next10Days = weatherData?.dailyForecastWeatherDataForNextMax10Days {
            for (key,data) in next10Days {
                self.next10Days[key] = data
            }
            sortedKeysForNext10Days = next10Days.keys.sorted()
        }
        
        self.collectionView.reloadData()
        self.stickyHeaderCell?.hourlyColletionView.collectionView.reloadData()
        
        showView(type: .Weather)
    }
    
    private func showView(type: ViewTypes) {
        switch type {
        case .Weather:
            collectionView.reloadData()
            collectionView.isHidden = false
            errorView.isHidden = true
            break
        case .ErrorDenied:
            collectionView.isHidden = true
            errorView.isHidden = false
            errorViewText.text = textDenied
            errorViewButon.isHidden = false
            break
        case .ErrorRestricted:
            collectionView.isHidden = true
            errorView.isHidden = false
            errorViewText.text = textRestricted
            errorViewButon.isHidden = true
            break
        }
    }
    
    private enum ViewTypes {
        case Weather
        case ErrorDenied
        case ErrorRestricted
    }
    
    private func setDailyForecastForNext10Days() {
        if let next10Days = weatherData?.dailyForecastWeatherDataForNextMax10Days {
            for (key,data) in next10Days {
                self.next10Days[key] = data
            }
            sortedKeysForNext10Days = next10Days.keys.sorted()
        }
    }
    
    func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        if let layout = self.collectionView.collectionViewLayout as? IOStickyHeaderFlowLayout {
            layout.parallaxHeaderReferenceSize = CGSize(width: UIScreen.main.bounds.size.width, height: stickyHeaderMaxHeight)
            layout.parallaxHeaderMinimumReferenceSize = CGSize(width: UIScreen.main.bounds.size.width, height: stickyHeaderMinHeight)
            layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width, height: layout.itemSize.height)
            layout.parallaxHeaderAlwaysOnTop = true
            layout.disableStickyHeaders = true
            self.collectionView.collectionViewLayout = layout
        }
        
        self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        self.collectionView.register(self.headerNib, forSupplementaryViewOfKind: IOStickyHeaderParallaxHeader, withReuseIdentifier: "header")
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Setting header animation
        if let header = stickyHeaderCell {
            let pointsToHide: CGFloat = 80
            header.stackTemp.alpha = alphaRatio(currentValue: header.frame.height, fromValue: stickyHeaderMaxHeight, toValue: stickyHeaderMaxHeight-pointsToHide)
            header.distanceFromTop.constant = alphaRatio(currentValue: header.frame.height, fromValue: stickyHeaderMaxHeight, toValue: stickyHeaderMinHeight) * 50 + 30
            header.lblTempMax.alpha = alphaRatio(currentValue: header.frame.height, fromValue: stickyHeaderMaxHeight, toValue: stickyHeaderMaxHeight-pointsToHide)
            header.lblTempMin.alpha = alphaRatio(currentValue: header.frame.height, fromValue: stickyHeaderMaxHeight, toValue: stickyHeaderMaxHeight-pointsToHide)
            header.lblToday.alpha = alphaRatio(currentValue: header.frame.height, fromValue: stickyHeaderMaxHeight, toValue: stickyHeaderMaxHeight-pointsToHide)
        }
    }
    
    func alphaRatio(currentValue current: CGFloat, fromValue from: CGFloat, toValue to:CGFloat) -> CGFloat {
        var alpha: CGFloat = 1
        let range = from - to
        
        if from > current && current > to {
            alpha = (current - to) / range
        } else if current <= to {
            alpha = 0
        } else if current >= from {
            alpha = 1
        }
        
        return alpha
    }
}

// MARK: UICollectionViewDataSource

extension WeatherVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.next10Days.count > 0 {
            return self.next10Days.count + 1
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // If last element
        if indexPath.row == sortedKeysForNext10Days.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weatherFooterCell", for: indexPath) as! WeatherFooterCell
            
            cell.weatherData = weatherData
            cell.updateCell()
            
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! WeatherHeaderCell
        
        let currentTime = sortedKeysForNext10Days[indexPath.row]
        cell.dayInterval = currentTime
        if let data = next10Days[currentTime] {
            cell.tempMax = data.tempMax
            cell.tempMin = data.tempMin
            cell.weatherIconName = data.weatherIcon
        }
        
        cell.updateCell()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case IOStickyHeaderParallaxHeader:
            let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! StickyHeaderCell
            
            self.stickyHeaderCell = cell
            
            cell.weatherData = weatherData
            cell.updateCell()
            return cell
        default:
            assert(false, "Unexpected element kind")
        }
    }
}

// MARK: UICollectionViewDelegate

extension WeatherVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == sortedKeysForNext10Days.count {
            return CGSize(width: UIScreen.main.bounds.size.width, height: 250) // PODESI OVO LJUDSKIIiIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
        }
        return CGSize(width: UIScreen.main.bounds.size.width, height: 25)
    }

}
