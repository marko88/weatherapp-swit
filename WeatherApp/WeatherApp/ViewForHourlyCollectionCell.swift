//
//  ViewForHourlyCollectionCell.swift
//  
//
//  Created by Marko Zec on 10/27/16.
//
//

import UIKit

class ViewForHourlyCollectionCell: UICollectionViewCell {
    
    var paretFrame = CGRect()
    var dt = 0
    var oneHourWeatherData: OneHourWeatherData?

    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var lblTemp: UILabel!
    
    @IBOutlet weak var heightLblDay: NSLayoutConstraint!
    @IBOutlet weak var heightLblTime: NSLayoutConstraint!
    @IBOutlet weak var heightLblTemp: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    func updateCell() {
        heightLblDay.constant = paretFrame.height / 3.5
        heightLblTime.constant = paretFrame.height / 5
        heightLblTemp.constant = paretFrame.height / 3.5
        
        self.backgroundColor = UIColor.clear
        
        if let data = oneHourWeatherData {
            let date = Date(timeIntervalSince1970: Double(dt))
            lblDay.text = Localization.getShortDayName(date: date)
            lblTime.text = Localization.getShortTime(date: date)
            
            let convertedTemp = WeatherDataManager.convertTempTo(unit: Settings.temperatureScale, temp: data.temp)
            lblTemp.text = "\(String(describing: convertedTemp))\u{00B0}"
            
            weatherIcon.image = UIImage(named: data.weatherIcon)
        }
    }
}
