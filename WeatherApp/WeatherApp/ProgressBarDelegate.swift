//
//  ProgressBarDelegate.swift
//  WeatherApp
//
//  Created by Marko Zec on 11/1/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation

protocol ProgressBarDelegate: class {
    func updateProgress(progress: Double) -> Void
    func progressionIsFinished() -> Void
}
